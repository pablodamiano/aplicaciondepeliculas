import 'package:aplicacion_de_peliculas/src/models/pelicula_model.dart';
import 'package:aplicacion_de_peliculas/src/providers/pelicula_provider.dart';
import 'package:flutter/material.dart';

class DataSearch extends SearchDelegate{
final  peliculasProvider = new PeliculasProvider(); 
String seleccion = '';
final peliculas = [
'spiderman',
'aquaman',
'batman',
'ironman 1',
'shzam',
'capitanamerica',
'ironman 2',
'ironman 3',
'ironman  4',
];

final peliculasRecientes = [
 'spiderman',
 'capitanamerica',
];
  @override
  List<Widget> buildActions(BuildContext context) {
   // Las Acciones de nuestro AppBar
    //throw UnimplementedError();
    return [
      IconButton(
        icon:Icon( Icons.clear), 
        onPressed: (){
         query='';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    //Icono a la izquierda de nuestro AppBar
    //throw UnimplementedError();
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow, 
        progress: transitionAnimation,
      ),
      onPressed: (){
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    //Crea los resultados  que vamos a mostrar
    //throw UnimplementedError();
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        color: Colors.blueAccent,
        child: Text(seleccion),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Son las sugerencias que aparecen cuando la persona escribe
    //throw UnimplementedError();
    if (query.isEmpty) {
      return Container();
    }
   return FutureBuilder(
     future: peliculasProvider.buscarPleicula(query) ,
     
     builder: (BuildContext context, AsyncSnapshot<List<Pelicula>> snapshot) {
       if(snapshot.hasData){
         final peliculas = snapshot.data;
         return ListView(
           children: peliculas.map(
             (pelicula){
               return ListTile(
                 leading:  FadeInImage(
                   placeholder: AssetImage('assets/img/no-image.jpg'), 
                   image: NetworkImage(pelicula.getPosterImg()),
                   width: 50.0,
                   fit: BoxFit.contain,
                  ),
                  title: Text(pelicula.title),
                  subtitle: Text(pelicula.originalTitle),
                  onTap: () {
                    close(context, null);
                    pelicula.uniqueID = '';
                    Navigator.pushNamed(context, 'detalle', arguments: pelicula);
                  },
               );
             }
           ).toList(),
         );
       }else{
         return Center(
           child:  CircularProgressIndicator(),
         );
       }
      
     },
   );
  }

} 