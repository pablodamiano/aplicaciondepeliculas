import 'package:aplicacion_de_peliculas/src/models/pelicula_model.dart';
import 'package:flutter/material.dart';

class MovieHorizontal extends StatelessWidget {

  final List<Pelicula> peliculas;
  final Function siguientePagina;

  MovieHorizontal({@required this.peliculas, @required this.siguientePagina});
    final _pageController = PageController(
              initialPage: 1,
              viewportFraction: 0.3,
    );

  @override
  Widget build(BuildContext context) {

    final _screensized = MediaQuery.of(context).size;
    
    _pageController.addListener(() { 
      if (_pageController.position.pixels >= _pageController.position.maxScrollExtent-200.0) {
        siguientePagina();
      }
    });
    return Container(
      height: _screensized.height * 0.2,
      child: PageView.builder(
        pageSnapping: false,
        controller: _pageController, 
        itemBuilder: (context, i) =>_tarjeta(context, peliculas[i]),
        
        itemCount: peliculas.length,
        ///children: _tarjetas(context),
      ),
    );
  }

  Widget _tarjeta(BuildContext context, Pelicula pelicula){
     pelicula.uniqueID = '${ pelicula.id}-poster';
     final tarjeta =  Container(     
        margin:  EdgeInsets.only(right: 15.0),
        child: Column(
          children: <Widget>[
            Hero(
              tag:  pelicula.uniqueID,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: FadeInImage(
                  fit:BoxFit.cover,
                  height: 125.0,
                  placeholder: AssetImage('assets/img/no-image.jpg'), 
                  image: NetworkImage(pelicula.getPosterImg(),
                  ),
                ),
              ),
            ),
            SizedBox(height:5.0,),
            Text(
              pelicula.title,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.caption,
            )
          ],
        ),
      );

      return GestureDetector(
        child: tarjeta,
        onTap: (){

          Navigator.pushNamed(context, 'detalle',arguments: pelicula );

          print('Id de la pelicula ${pelicula.id}');
          
          },
      );
  }

  List<Widget> _tarjetas(BuildContext context) {
    return peliculas.map((pelicula){
     
    }).toList();  
  }
}