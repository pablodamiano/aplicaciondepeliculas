import 'package:aplicacion_de_peliculas/src/models/pelicula_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class CardSwiper extends StatelessWidget {

  final List<Pelicula> peliculas;
  CardSwiper({ @required this.peliculas});
  
  @override
  Widget build(BuildContext context) {

    final _screensized = MediaQuery.of(context).size;

   return Container(
      padding: EdgeInsets.only(top: 10.0), 
      child: Swiper(
            itemWidth: _screensized.width*0.65,
            itemHeight: _screensized.height*0.45,
            layout:SwiperLayout.STACK,
            itemBuilder: (BuildContext context,int index){
              peliculas[index].uniqueID = '${ peliculas[index].id}-tarjetas';
              return Hero(
                tag: peliculas[index].uniqueID,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child:GestureDetector(
                    onTap: () {
                       Navigator.pushNamed(context, 'detalle',arguments: peliculas[index]);
                    },
                    child: FadeInImage(
                      placeholder: AssetImage('assets/img/no-image.jpg'), 
                      image: NetworkImage(peliculas[index].getPosterImg()),
                      fit: BoxFit.cover,
                      ),
                  ),
                  //Image.network("http://via.placeholder.com/350x150",fit: BoxFit.cover,),
                ),
              );
            },
            itemCount: peliculas.length,
            //pagination: new SwiperPagination(),
           // control: new SwiperControl(),
          ),
   );
  }
}