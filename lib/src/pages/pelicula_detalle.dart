import 'package:aplicacion_de_peliculas/src/models/actores_model.dart';
import 'package:aplicacion_de_peliculas/src/models/pelicula_model.dart';
import 'package:aplicacion_de_peliculas/src/providers/pelicula_provider.dart';
import 'package:flutter/material.dart';

class PeliculasDetalle extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final Pelicula pelicula = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body:CustomScrollView(
        slivers: <Widget>[
          _crearAppbar(pelicula),
          SliverList(
            delegate: SliverChildListDelegate(
                [
                  SizedBox(width: 10.0,),
                  _posterTitulo(pelicula, context),
                  _descripcion(pelicula),
                  _descripcion(pelicula),  
                  _descripcion(pelicula),
                  _descripcion(pelicula),
                  _descripcion(pelicula),  
                  _descripcion(pelicula),
                  _crearcasting(pelicula),
                ]
            ),
          ),
        ],
      ),
    );
  }

  Widget _crearAppbar(Pelicula pelicula) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle:  true,
        title: Text(
          pelicula.title,
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          ),
        ),
        background: FadeInImage(         
          placeholder: AssetImage('assets/img/loading.gif'),
          image:NetworkImage(pelicula.getBackgroundImg()),
          fadeInDuration: Duration(milliseconds: 500),
          fit: BoxFit.cover,

          ),
      ),
    );
  }

  Widget _posterTitulo(Pelicula pelicula,BuildContext context) {

    return Container(
      padding:  EdgeInsets.symmetric(
        horizontal: 20.0,
        vertical: 10.0,
      ),
      child: Row(
        children: <Widget>[
          Hero(
            tag: pelicula.id,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: NetworkImage(pelicula.getPosterImg()),
                height: 150.0,
              ),
            ),
          ),
          SizedBox(width: 20.0,),
          Flexible(
            child:Column(             
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(pelicula.title, style: Theme.of(context).textTheme.headline6,overflow: TextOverflow.ellipsis,),
                Text(pelicula.originalTitle, style: Theme.of(context).textTheme.subtitle1,overflow: TextOverflow.ellipsis,),
                Row(
                  children: <Widget>[
                    Icon(Icons.star_border),
                    Text(pelicula.voteAverage.toString(),style:Theme.of(context).textTheme.subtitle1,),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _descripcion(Pelicula pelicula) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 20.0,
        vertical: 10.0,
      ),
      child: Text(
        pelicula.overview,
        textAlign: TextAlign.justify,
      ),
    );
  }

  Widget _crearcasting(Pelicula pelicula) {
    final peliProvider = PeliculasProvider();
    return FutureBuilder(
      future: peliProvider.getCast(pelicula.id.toString()),      
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
         // print(snapshot.data.forEach((element) {print(element);}));
          return _crearActoresPageview(snapshot.data);
          
        }else{
          return CircularProgressIndicator();
        }
      },
    );
  }

  Widget _crearActoresPageview(List<Actor> actores ) {

    return SizedBox(
      height: 200.0,
      child: PageView.builder(
        controller: PageController(
          keepPage: false,
          viewportFraction: 0.3,
          initialPage: 1,
        ),
        itemBuilder:(context,  i){
         return _actorTarjeta(actores[i]);
        },
        itemCount: actores.length,
      ),
    );

  }
  Widget _actorTarjeta(Actor actor){
    return Container(
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: FadeInImage(
              placeholder: AssetImage('assets/img/no-image.jpg'), 
              image: NetworkImage(actor.getPhoto()),
              height: 150.0,
              width: 100,
              fit: BoxFit.cover,
            ),  
          ),
          Text(
            actor.name,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}