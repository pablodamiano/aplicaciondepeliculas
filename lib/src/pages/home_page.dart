import 'package:aplicacion_de_peliculas/src/Widgets/car_swiper_widget.dart';
import 'package:aplicacion_de_peliculas/src/Widgets/movie_horizontal.dart';

import 'package:aplicacion_de_peliculas/src/providers/pelicula_provider.dart';
import 'package:aplicacion_de_peliculas/src/search/search_delegate.dart';
import 'package:flutter/material.dart';

class HomePge extends StatelessWidget {
   final peliculasprovider = new PeliculasProvider();
  @override
  Widget build(BuildContext context) {

    peliculasprovider.getPopulares();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('Peliculas en cines'),
        backgroundColor: Colors.indigoAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search), 
            onPressed: (){
              showSearch(
                context: context, 
                delegate: DataSearch(),
                //query: 'Hola',
              );
            })
        ],

      ),
      body:Container(      
        child: Column(
          mainAxisAlignment:  MainAxisAlignment.spaceAround,
          children: <Widget>[
            _swiperTarjetas(),
            _footer(context),
          ],
        ),
      )
    );
  }

 Widget _swiperTarjetas() {

   return FutureBuilder(
     future: peliculasprovider.getEnCines(),
     builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
       if (snapshot.hasData) {
          return CardSwiper(
          peliculas:snapshot.data,
          );
       }else{
         return Container(
           height:400.0,
            child: Center(
              child: CircularProgressIndicator(

              ),
            ),
         );
       }
        
     },
   );
   //final peliculasprovider = PeliculasProvider();
   //peliculasprovider.getEnCines();
    //return CardSwiper(
     // peliculas: [1,2,3,4]
     // );
 }

  Widget _footer(BuildContext context) {

    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      width: double.infinity,
      child:  Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
           padding: EdgeInsets.only(left:20.0), 
            child: Text('Populares' ,
            style: Theme.of(context).textTheme.subtitle1
            ),
           ),
          SizedBox(height: 5.0,),
          StreamBuilder(
            stream: peliculasprovider.popularesStream,           
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {

              if(snapshot.hasData){
                  return MovieHorizontal(
                    peliculas: snapshot.data,
                    siguientePagina: peliculasprovider.getPopulares,
                    );
                // snapshot.data.forEach((element) {     
                // print(element.backdropPath);
                //});
              }else{
                  return Center(child: CircularProgressIndicator());
              }
 
            },
          ),
        ],
      ),
    );
  }
}